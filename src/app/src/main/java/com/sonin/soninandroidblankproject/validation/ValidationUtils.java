package com.sonin.soninandroidblankproject.validation;



public class ValidationUtils {

	public static boolean validateEmail(String strValidate) {

		if (strValidate.length() < 1) {
			return true;
		}

		EmailValidator ev = EmailValidator.getInstance();
		if (ev.isValid(strValidate) == false) {
			return false;
		} else {
			return true;
		}

	}

	public static boolean validateEmpty(String strValidate) {
		if (strValidate == null || strValidate.length() == 0  || strValidate.trim().length() == 0) {
			return false;
		}
		return true;
	}

	public static boolean validateNumeric(String strValidate) {

		if (strValidate.length() > 0) {
			try {
				Integer.parseInt(strValidate);
				return true;
			} catch (NumberFormatException ex) {
				return false;
			}
		}
		return true;
	}

	public static PasswordPolicy.PasswordStrength calculateStrength(String password) {

		int currentScore = 0;
		boolean sawUpper = false;
		boolean sawLower = false;
		boolean sawDigit = false;
		boolean sawSpecial = false;

		// The first time the length passes 6, we increment the score.
		if (password.length() > 6)
			currentScore += 1;

		// Do this as efficiently as possible.
		for (int i = 0; i < password.length(); i++) {
			char c = password.charAt(i);
			if (!sawSpecial && !Character.isLetterOrDigit(c)) {
				currentScore += 1;
				sawSpecial = true;
			} else {
				if (!sawDigit && Character.isDigit(c)) {
					currentScore += 1;
					sawDigit = true;
				} else {
					if (!sawUpper || !sawLower) {
						if (Character.isUpperCase(c))
							sawUpper = true;
						else
							sawLower = true;
						if (sawUpper && sawLower)
							currentScore += 1;
					}
				}
			}
		}

		switch (currentScore) {
		case 0:
			return PasswordPolicy.PasswordStrength.WEAK;
		case 1:
			return PasswordPolicy.PasswordStrength.MEDIUM;
		case 2:
			return PasswordPolicy.PasswordStrength.MEDIUM;
		case 3:
			return PasswordPolicy.PasswordStrength.STRONG;
		default:
			return PasswordPolicy.PasswordStrength.VERY_STRONG;
		}

	}

}
