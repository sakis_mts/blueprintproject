package com.sonin.soninandroidblankproject.dagger

import android.app.Application
import com.sonin.soninandroidblankproject.database.AppDatabase
import android.arch.persistence.room.Room
import com.sonin.soninandroidblankproject.database.TestModelDao
import com.sonin.soninandroidblankproject.database.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomDatabaseModule(val mApplication: Application) {


    @Singleton
    @Provides
    fun providesRoomDatabase(): AppDatabase {
        return  AppDatabase.getDatabase(mApplication)
    }

    @Singleton
    @Provides
    fun providesProductDao(appDatabase: AppDatabase): UserDao {
        return appDatabase.userModel()
    }

    @Singleton
    @Provides
    fun providesTestModelDao(appDatabase: AppDatabase): TestModelDao {
        return appDatabase.testModel()
    }
}