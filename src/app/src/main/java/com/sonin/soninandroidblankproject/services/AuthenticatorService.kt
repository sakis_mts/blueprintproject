package com.sonin.soninandroidblankproject.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.sonin.soninandroidblankproject.BuildConfig
import com.sonin.soninandroidblankproject.application.AccountAuthenticator
import com.sonin.soninandroidblankproject.views.LoginActivity



internal class AuthenticatorService : Service() {

    override fun onBind(intent: Intent): IBinder {

        val authenticator = AccountAuthenticator(this, LoginActivity::class.java, BuildConfig.ENDPOINT)

        return authenticator.getIBinder()
    }
}