package com.sonin.soninandroidblankproject.dagger

import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AuthenticatorException
import android.accounts.OperationCanceledException
import android.content.Context
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.application.AccountGeneral
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.networking.CustomMoshiConverterFactory
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidblankproject.networking.OAuthService
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by athanasios.moutsioul on 22/02/2018.
 */
const val SUBCRIBER_ON = "SubscribeOn"
const val OBSERVER_ON = "ObserverOn"

@Module
class RetrofitModule (val strEndpoint: String, val timeout: Long, var accessToken: String?=null, val strUserName: String?=null, val accountType: String?=null, var converter : Converter.Factory?= null) {

    companion object {

        var strGrandType                = "password"
        var strGrandTypeRefreshToken    = "refresh_token"
    }
    var intRefreshCounter               = 0

    @Provides
    @Singleton
    @Named("WithoutAccessToken")
    fun provideOkhttpClientWithoutAccessToken(): OkHttpClient {

        val okHttpBuilder       : OkHttpClient.Builder      =  OkHttpClient.Builder()
        val interceptorLogging  : HttpLoggingInterceptor    =  HttpLoggingInterceptor()

        interceptorLogging.level = HttpLoggingInterceptor.Level.BODY

        val interceptor         : Interceptor = Interceptor {

            it.proceed(it.request().newBuilder().addHeader("Accept", "application/json").build())

        }

        okHttpBuilder.addInterceptor(interceptor)
        okHttpBuilder.addInterceptor(interceptorLogging).build();

        val okHttpClient    = okHttpBuilder.readTimeout(30,TimeUnit.SECONDS).writeTimeout(30,TimeUnit.SECONDS).build()
        return okHttpClient
    }

    @Provides
    @Singleton
    @Named("WithAccessToken")
    fun provideOkhttpClientWithAccessToken(accountManager: AccountManager): OkHttpClient {

        val interceptorLogging      = HttpLoggingInterceptor()
        interceptorLogging.level    = HttpLoggingInterceptor.Level.BODY
        val okHttpBuilder           = OkHttpClient.Builder()

        //add headers
        val interceptor         : Interceptor = Interceptor {

            val tmp = SessionManager.instance.getAuthorizationAccessTokenType()
            println(SessionManager.instance.getAuthorizationAccessTokenType())
            it.proceed(it.request().newBuilder()
                    .removeHeader("Authorization")
                    .addHeader("Authorization", SessionManager.instance.getAuthorizationAccessTokenType())
                    .header("Accept", "application/json")
                    .build())

        }



        accessToken?.let {

            okHttpBuilder.authenticator { route, response ->

                intRefreshCounter++
                if (intRefreshCounter > 3) {

                    println("Refresh failed")
                    SessionManager.instance.logout(MyApp.instance)
                    null

                } else {

                    accountType?.let { type->


                        val account = AccountGeneral.getAccountFromUsername(accountManager, SessionManager.instance.getLoggedInUsername(), type)

                        account?.let { userAccount->

                            invalidateAuthToken(accountManager, userAccount, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS)

                            this@RetrofitModule.accessToken = SessionManager.instance.getAuthorizationAccessTokenType()

                            val request = response.request().newBuilder()
                                    .header("Authorization", SessionManager.instance.getAuthorizationAccessTokenType().trim { it <= ' ' })
                                    .build()
                             request
                        }

                    }

                }
            }

        }

        okHttpBuilder.addNetworkInterceptor(interceptor);
        okHttpBuilder.addInterceptor(interceptorLogging).build();
        okHttpBuilder.cache(null);
        var okHttpClient    = okHttpBuilder.readTimeout(timeout,TimeUnit.SECONDS).writeTimeout(timeout,TimeUnit.SECONDS).build()
        return okHttpClient
    }


    fun invalidateAuthToken(mAccountManager: AccountManager, account: Account, authTokenType: String) {

        //delete auth token from cache
        mAccountManager.invalidateAuthToken(account.type, mAccountManager.peekAuthToken(account, authTokenType))

        try {
            //request a new access token
            mAccountManager.blockingGetAuthToken(account, authTokenType, false)
        } catch (e: OperationCanceledException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: AuthenticatorException) {
            e.printStackTrace()
        }

    }

    @Provides
    @Singleton
    fun provideAccountManager(context: Context): AccountManager {
       return AccountManager.get(context)
    }



    @Provides
    @Singleton
    @Named("RetrofitWithoutAccessToken")
    fun provideRetrofitWithoutAccessToken(@Named("WithoutAccessToken") okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(strEndpoint)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build()


    }



    @Provides
    @Singleton
    @Named("RetrofitWithAccessToken")
    fun provideRetrofitWithAccessToken(@Named("WithAccessToken") okHttpClient: OkHttpClient): Retrofit {

        var responseConverter =  CustomMoshiConverterFactory.create() as Converter.Factory

        if (converter!=null){

            responseConverter = converter as Converter.Factory

        }

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(strEndpoint)
                .addConverterFactory(responseConverter)
                .client(okHttpClient)
                .build()

    }

    @Provides
    @Singleton
    fun provideItemServiceWithoutAccessToken(@Named("RetrofitWithoutAccessToken")  retrofit: Retrofit): OAuthService {
        return retrofit.create<OAuthService>(OAuthService::class.java!!)
    }

    @Provides
    @Singleton
    fun  provideItemServiceWithAccessToken(@Named("RetrofitWithAccessToken") retrofit: Retrofit): NetworkRequests {

        return retrofit.create<NetworkRequests>(NetworkRequests::class.java!!)

    }


    ///// Testtttt////
    @Provides
    @Singleton
    @Named("ServiceRetrofitTest")
    fun  provideServiceItemServiceWithoutAccessToken(@Named("RetrofitTest") retrofit: Retrofit): NetworkRequests {

        return retrofit.create<NetworkRequests>(NetworkRequests::class.java!!)

    }

    @Provides
    @Singleton
    @Named("TestOkhttpClient")
    fun provideTestOkhttpClient(): OkHttpClient {

        val okHttpBuilder       : OkHttpClient.Builder      =  OkHttpClient.Builder()
        val interceptorLogging  : HttpLoggingInterceptor    =  HttpLoggingInterceptor()

        interceptorLogging.level = HttpLoggingInterceptor.Level.BODY

        val interceptor         : Interceptor = Interceptor {

            it.proceed(it.request().newBuilder().addHeader("Accept", "application/json").build())

        }

        okHttpBuilder.addInterceptor(interceptor)
        okHttpBuilder.addInterceptor(interceptorLogging).build();

        val okHttpClient    = okHttpBuilder
                .readTimeout(30,TimeUnit.SECONDS)
                .writeTimeout(30,TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS).build()
        return okHttpClient
    }

    @Provides
    @Singleton
    @Named("RetrofitTest")
    fun provideTestRetrofit(@Named("TestOkhttpClient") okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(strEndpoint)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build()


    }

}