package com.sonin.soninandroidblankproject.dagger

import com.sonin.soninandroidblankproject.application.AccountAuthenticator
import com.sonin.soninandroidblankproject.database.UserDao
import com.sonin.soninandroidblankproject.models.User
import com.sonin.soninandroidblankproject.repository.DataRepository
import com.sonin.soninandroidblankproject.viewModel.LoginViewModel
import com.sonin.soninandroidblankproject.viewModel.NavigationViewModel
import com.sonin.soninandroidblankproject.views.LoginActivity
import com.sonin.soninandroidblankproject.views.LoginFragment
import com.sonin.soninandroidblankproject.views.NavigationActivity
import dagger.Component
import io.github.philippeboisney.retrokotlin.di.ViewModelModule
import javax.inject.Singleton

/**
 * Created by athanasios.moutsioul on 23/02/2018.
 */
@Singleton
@Component(modules = [AppModule::class, RetrofitModule::class, RoomDatabaseModule::class, DataRepoModule::class, ViewModelModule::class, RxJavaModule::class])

interface AppComponent {

    fun inject(target: AccountAuthenticator)
    fun inject(target: DataRepository)
    fun inject(target: LoginFragment)
    fun inject(target: NavigationActivity)


}