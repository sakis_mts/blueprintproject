package com.sonin.soninandroidblankproject.validation;

import android.view.View;
import android.view.animation.Animation;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Form {
	
	public interface CustomFailureCallback
	{
		void onFormFailed(View view, String strFailureMessage);
	}

	private String strAction;
	private ArrayList<Field> arrFields;
	private boolean               blnDisplayError;
	private CustomFailureCallback customFailureCallback;
	private Animation customAnimation;
	
	
	
	
	public Form ()
	{
		blnDisplayError 	= false;
		arrFields 			= new ArrayList<Field>();
	}
	
	
	public boolean validateForm(){
		
		int size = arrFields.size();
		
		for(int i = 0; i<size; i++)
		{
			Field field = arrFields.get(i);

			//Clear Previous Errors
			if(blnDisplayError)
			{
				if(field.getView() instanceof TextView)
				{
					((TextView) field.getView()).setError(null);
				}else if(field.getView() instanceof Spinner) //TODO clear error for spinner
				{

				}
			}


			if(arrFields.get(i).validateField() == false)
			{
				generateFailure(field.getView(), field.getStrFailure());
				return false;
			}
		}
		
		return true;
	}
	

	
	
	private void generateFailure(View view, String strFailureMessage) {
		
		view.requestFocus();
		
		if(this.customFailureCallback!=null)
		{
			customFailureCallback.onFormFailed(view, strFailureMessage);
		}
		
		if(customAnimation != null)
		{
			view.startAnimation(customAnimation);
		}
		
		if(this.blnDisplayError)
		{
			if(view instanceof TextView)
			{
				((TextView) view).setError(strFailureMessage);
			}else if(view instanceof Spinner) //TODO display error for spinner
			{

			}
		}
	}
	
	
	public boolean validateFieldWithId(int id)
	{
		
		int size = arrFields.size();
		
		for(int i =0; i<size; i++){
			
			if(arrFields.get(i).getView().getId() == id)
			{
				if(arrFields.get(i).validateField() == false){
					generateFailure(arrFields.get(i).getView(), arrFields.get(i).getStrFailure());
					return false;
				}
			}
		}
		return true;
	}
	
	public JSONObject toJSON()
	{
		JSONObject jsonObject = new JSONObject();

		try{

			if(strAction != null)
			{
				jsonObject.put("strAction", strAction);
			}

			int size = arrFields.size();
			for(int i =0; i<size; i++)
			{
				if(arrFields.get(i).getRequestKey()!=null)
				{
					jsonObject.put(arrFields.get(i).getRequestKey(), arrFields.get(i).getString());
				}
			}
		}catch(JSONException e){
			
		}
		return jsonObject;
	}

	public HashMap<String, String> toHashMap()
	{
		HashMap<String,String> formMap = new HashMap<String, String>();

		if(strAction != null)
		{
			formMap.put("strAction", strAction);
		}

		int size = arrFields.size();
		for(int i =0; i<size; i++)
		{
			if(arrFields.get(i).getRequestKey()!= null)
			{
				formMap.put(arrFields.get(i).getRequestKey(), arrFields.get(i).getString());
			}
		}
		return formMap;
	}

	public void setAction(String strAction)
	{
		this.strAction = strAction;
	}

	public void addField(Field field){
		arrFields.add(field);
	}

	public void setDisplayError(boolean b) {
		this.blnDisplayError = true;
	}
	
	public void setCustomFailure(CustomFailureCallback customFailure){
		this.customFailureCallback = customFailure;
	}
	
	public void setCustomFailureAnimation(Animation animation){
		this.customAnimation = animation;
	}

	
	
	
	

}
