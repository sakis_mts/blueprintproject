package com.sonin.soninandroidblankproject.application

import android.app.Application
import android.provider.ContactsContract
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.BuildConfig
import com.sonin.soninandroidblankproject.dagger.*
import com.sonin.soninandroidblankproject.networking.CustomMoshiConverterFactory
import com.sonin.soninandroidblankproject.repository.DataRepository
import com.sonin.soninandroidblankproject.views.LoginActivity
import com.sonin.soninandroidblankproject.views.NavigationActivity

/**
 * Created by athanasios.moutsioul on 10/04/2018.
 */
open class MyApp: Application() {

    companion object {
        lateinit var instance: MyApp
            private set
    }

    var retrofitComponent   : AppComponent? = null

    protected open fun initDagger(app: MyApp,module: RetrofitModule?): AppComponent {
        if (module!=null){
            return DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .roomDatabaseModule(RoomDatabaseModule(app))
                    .retrofitModule(module)
                    .rxJavaModule(RxJavaModule())
                    .dataRepoModule(DataRepoModule())
                    .build()

        }else{
            return  DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .roomDatabaseModule(RoomDatabaseModule(app))
                    .dataRepoModule(DataRepoModule())
                    .rxJavaModule(RxJavaModule())
                    .retrofitModule(RetrofitModule(BuildConfig.ENDPOINT, 30))
                    .build()

        }

    }


    override fun onCreate() {
        super.onCreate()

        instance        = this

        //init Session Manager
        SessionManager.init(this,NavigationActivity::class.java, LoginActivity::class.java, null )

        //init Dagger Retrofit Module
        if (SessionManager.instance.isLoggedIn()){

            val module              = RetrofitModule(BuildConfig.ENDPOINT, 30,SessionManager.instance.getAccessToken(),SessionManager.instance.getLoggedInUsername(),SessionManager.instance.getAccessTokenType(),CustomMoshiConverterFactory.createWithNull())
            retrofitComponent       = initDagger(this,module)

        }else{

            retrofitComponent       = initDagger(this,null)

        }

    }


    fun updateDagger(accessToken: String, strUsername:String,accountType:String ){

        retrofitComponent   = null
        val module          = RetrofitModule(BuildConfig.ENDPOINT, 30,accessToken,strUsername,accountType, CustomMoshiConverterFactory.createWithNull())
        retrofitComponent   = initDagger(this, module)


    }
}