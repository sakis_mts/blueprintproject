package com.sonin.soninandroidblankproject.networking

import com.sonin.soninandroidblankproject.models.TestModel
import com.sonin.soninandroidblankproject.models.User
import java.util.*
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by athanasios.moutsioul on 19/10/2017.
 */
interface NetworkRequests {

    @GET("api/getUser")
    fun getUser( ): Single<User>

}