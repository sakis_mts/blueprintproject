package com.sonin.soninandroidblankproject.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "AccessToken")
data class AccessToken(

        @SerializedName("token_type")
        var token_type          : String? ,

        @SerializedName("expires_in")
        var expires_in          : Int?    ,

        @PrimaryKey(autoGenerate = false)
        @SerializedName("access_token")
        var access_token        : String ,

        @SerializedName("refresh_token")
        var refresh_token       : String?

){
        constructor() : this(null, null,"",null)
}

