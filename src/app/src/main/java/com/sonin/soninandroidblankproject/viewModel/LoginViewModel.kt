package com.sonin.soninandroidblankproject.viewModel


import android.accounts.AccountManager
import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.os.AsyncTask
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.View
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.R
import com.sonin.soninandroidblankproject.application.AccountGeneral
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.dagger.OBSERVER_ON
import com.sonin.soninandroidblankproject.dagger.SUBCRIBER_ON
import com.sonin.soninandroidblankproject.models.AccessToken
import com.sonin.soninandroidblankproject.models.RetrofitErrorResponse
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidblankproject.networking.OAuthService
import com.sonin.soninandroidblankproject.repository.DataRepository
import com.sonin.soninandroidblankproject.utils.SingleLiveEvent
import com.sonin.soninandroidblankproject.utils.TangerineLog

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import org.json.JSONObject
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by athanasios.moutsioul on 18/10/2017.
 */
class LoginViewModel @Inject constructor(private val dataRepository: DataRepository, @param:Named(SUBCRIBER_ON) private val subscriberOn: Scheduler,
                                         @param:Named(OBSERVER_ON) private val observerOn: Scheduler, val app: Application) : AndroidViewModel(app) {


    var obsUsername                                     = ObservableField("")
    var obsPassword                                     = ObservableField("")
    private var compositeDisposable                     : CompositeDisposable

    private var context                                 : Context
    var obsProgrBar                                     = ObservableField(View.GONE)
    var accessToken: AccessToken?= null

    val showErrorToast = SingleLiveEvent<String>()

    init {

        val tmpUsername = "athan@sonin.com"
        val tmpPass     = "123456"

        obsUsername.set(tmpUsername)
        obsPassword.set(tmpPass)

        context             = getApplication()
        compositeDisposable = CompositeDisposable()


    }


    fun logingRequest(){

        TangerineLog.log("the email is : "+ obsUsername.get())
        obsProgrBar.set(View.VISIBLE)
        compositeDisposable.add(
                dataRepository.getAccessToken(obsUsername.get()!!,obsPassword.get()!!)
                        .observeOn(observerOn)
                        .subscribeOn(subscriberOn)
                        .subscribe ({
                            result ->
                            accessToken = result
                            obsProgrBar.set(View.GONE)

                            SessionManager.instance.setAccount(context,result,context.getString(R.string.account_type),obsUsername.get().toString(),true, true)

                            val account = AccountGeneral.isAccountExists(AccountManager.get(context), obsUsername.get()!!.toString(), context.getString(R.string.account_type))

                            if (account != null) {

                                AccountManager.get(context).setPassword(account, result.refresh_token)
                            }

                            MyApp.instance.updateDagger(SessionManager.instance.getAuthorizationAccessTokenType(),obsUsername.get().toString(),context.getString(R.string.account_type))

                        }, { error ->


                            RetrofitErrorResponse.onError(error)?.let {

                                showErrorToast.value = it

                            }
                            Log.d("Error", " Error ")
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)
                        })


        )

    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }



}