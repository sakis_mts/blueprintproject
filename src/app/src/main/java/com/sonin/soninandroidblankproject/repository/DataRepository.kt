package com.sonin.soninandroidblankproject.repository
import com.sonin.soninandroidblankproject.dagger.OBSERVER_ON
import com.sonin.soninandroidblankproject.dagger.SUBCRIBER_ON
import android.arch.lifecycle.LiveData
import android.util.Log
import com.sonin.soninandroidblankproject.R
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.database.AppDatabase
import com.sonin.soninandroidblankproject.database.TestModelDao
import com.sonin.soninandroidblankproject.database.UserDao
import com.sonin.soninandroidblankproject.models.AccessToken
import com.sonin.soninandroidblankproject.models.TestModel
import com.sonin.soninandroidblankproject.models.User
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidblankproject.networking.OAuthService
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named


class DataRepository(@Named("ServiceRetrofitTest")  private val testServiceApi: NetworkRequests, private val networkServiceApi: NetworkRequests, private val oauthServiceApi: OAuthService, @Named(OBSERVER_ON) private val observerOn: Scheduler, @Named(SUBCRIBER_ON) private val subscriberOn : Scheduler)  {

     @Inject
     lateinit var appDatabase        : AppDatabase


     @Inject
     lateinit var userDao           : UserDao
     @Inject
     lateinit var testModelDao      : TestModelDao

     lateinit var arrUsers          : LiveData<List<User>>
     private val strGrandType       = "password"

     init {

         MyApp.instance?.retrofitComponent?.inject(this)

     }


     ///////////////   Users  /////////////////////////////
    fun getUserRequest(compositeDisposable: CompositeDisposable,mListener : UserCallBack? = null ){

        mListener?.let { listener->

            compositeDisposable.add(
                    getUser()
                            .observeOn(observerOn)
                            .subscribeOn(subscriberOn)
                            .subscribe ({
                                result ->

                                result?.let {

                                    it.insertItem(appDatabase)
                                }
                                listener.resultUser(result)

                            }, { error ->
                                Log.d("Error", " Error ")
                                error.printStackTrace()
                                listener.networkError(error)
                            })

            )


        }


    }

     fun getUsersFromDB(): LiveData<List<User>>{

          arrUsers = userDao.getAllUsers()

         return arrUsers

     }

     fun deleteFirstUserFromDB(){

         arrUsers.value?.let {
             if (it.size >0){

                 it[0].deleteItem(appDatabase)
             }

         }

     }


     //Api calls
     fun getAccessToken(username: String, password: String) : Observable<AccessToken> {

         return oauthServiceApi.getAccessTokennObservable(strGrandType, username, password, MyApp.instance.getString(R.string.oauth_client_id),  MyApp.instance.getString(R.string.oauth_client_secret))

     }

    private fun getUser() : Single<User> {

        return networkServiceApi.getUser()
    }


     //Interfaces

     interface UserCallBack {

         fun resultUser(user: User)
         fun networkError(error: Throwable )
     }

     interface TestModelCallBack {

         fun resultTestModel(testModel: List<TestModel>)
         fun networkTestModelError(error: Throwable )
     }

}