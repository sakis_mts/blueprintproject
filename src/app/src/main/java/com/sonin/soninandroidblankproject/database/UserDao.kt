package com.sonin.soninandroidblankproject.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.sonin.soninandroidblankproject.models.User

/**
 * Created by athanasios.moutsioul on 11/04/2018.
 */
@Dao
interface UserDao {

    @Query("select * from User")
    fun getAllUsers(): LiveData<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUser(accessTokenModel: User)

    @Delete
    fun deleteUser(accessTokenModel: User)
}