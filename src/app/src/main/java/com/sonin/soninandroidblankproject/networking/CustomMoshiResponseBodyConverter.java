package com.sonin.soninandroidblankproject.networking;

import com.google.gson.stream.JsonReader;
import com.sonin.soninandroidblankproject.utils.TangerineLog;
import com.squareup.moshi.JsonAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import okio.BufferedSource;
import okio.ByteString;
import retrofit2.Converter;

public class CustomMoshiResponseBodyConverter <T> implements Converter<ResponseBody, T> {
    private static final ByteString UTF8_BOM = ByteString.decodeHex("EFBBBF");

    private final JsonAdapter<T> adapter;

    CustomMoshiResponseBodyConverter(JsonAdapter<T> adapter) {
        this.adapter = adapter;
    }

    @Override public T convert(ResponseBody value) throws IOException {
        try {

            String strJsondata  = "";
            String strJson      = (String) value.string();
            JSONObject json     = new JSONObject(strJson);

            TangerineLog.Companion.log("Network Response: " + strJson);
            Object intervention =null;
            if (json.has("data")){
                intervention  = json.get("data");
            }else if (json.has("arrData")){
                intervention  = json.get("arrData");
            }

           if (intervention instanceof JSONArray){

            JSONArray jsonArray     = json.getJSONArray("arrData");
            strJsondata             = jsonArray.toString();

            }else{

                if (json.has("data")) {

                    JSONObject jsonObject = json.getJSONObject("data");
                    strJsondata = jsonObject.toString();

                }else if (json.has("email")){

                    JSONObject jsonObject = json.getJSONObject("email");
                    strJsondata = jsonObject.toString();

                }else {

                    throw new IOException("There isn't any object or array inside the response!");

                }


            }


            try {

                return  adapter.fromJson(strJsondata);

            } finally {

                value.close();

            }

        } catch (JSONException e) {

            throw new IOException("Failed to parse JSON", e);

        }
    }



}