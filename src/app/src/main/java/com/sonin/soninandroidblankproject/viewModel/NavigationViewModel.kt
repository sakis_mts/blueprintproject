package com.sonin.soninandroidblankproject.viewModel

import android.accounts.AccountManager
import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.databinding.ObservableField
import android.os.AsyncTask
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.dagger.OBSERVER_ON
import com.sonin.soninandroidblankproject.dagger.SUBCRIBER_ON
import com.sonin.soninandroidblankproject.database.AppDatabase
import com.sonin.soninandroidblankproject.models.TestModel
import com.sonin.soninandroidblankproject.models.User
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidblankproject.repository.DataRepository
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by athanasios.moutsioul on 11/04/2018.
 */
class NavigationViewModel@Inject constructor(private val dataRepository: DataRepository, @param:Named(SUBCRIBER_ON) private val subscriberOn: Scheduler,
                                             @param:Named(OBSERVER_ON) private val observerOn: Scheduler, val app: Application) : AndroidViewModel(app) , DataRepository.UserCallBack, DataRepository.TestModelCallBack {

    private var compositeDisposable                 = CompositeDisposable()
    private lateinit var activity                   : Activity
    private  var fragment                           : Fragment? = null
    lateinit var arrUsers                           : LiveData<List<User>>
    var obsName                                     = ObservableField("")
    var obsEmail                                    = ObservableField("")

    fun setCallingActivityFragment(activity: Activity, fragment: Fragment? = null){

        this.activity   = activity
        this.fragment   = null
        arrUsers        = dataRepository.getUsersFromDB()


    }
    override fun resultUser(user: User) {

        obsName.set(user.name)
        obsEmail.set(user.email)

    }

    fun loadFromDb(){

        arrUsers.value?.let { users->

            println(users.count())
            if (!users.isEmpty()){

                obsName.set(users.first().name)
                obsEmail.set(users.first().email)

            }else{
                obsName.set("")
                obsEmail.set("")

            }

        }



    }

    fun clear(){

        obsName.set("")
        obsEmail.set("")

    }

    override fun networkError(error: Throwable) {

    }

    fun getUserRequest(){

        dataRepository.getUserRequest(compositeDisposable, this)


    }

    override fun resultTestModel(testModel: List<TestModel>) {
        println("From network $testModel")
    }

    override fun networkTestModelError(error: Throwable) {

    }


    fun deleteUser(){

        dataRepository.deleteFirstUserFromDB()

    }


    fun onResume(){

        compositeDisposable = CompositeDisposable()

    }

    fun onDestroy() {

        compositeDisposable.clear()
    }

}