package com.sonin.soninandroidblankproject.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.sonin.soninandroidblankproject.models.AccessToken
import com.sonin.soninandroidblankproject.models.TestModel
import com.sonin.soninandroidblankproject.models.User
import com.sonin.soninandroidblankproject.utils.TangerineLog


@Database(entities = arrayOf(AccessToken::class, User::class, TestModel::class), version = 1)
abstract class AppDatabase : RoomDatabase() {



    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "testTracker_db").build()
            }
            TangerineLog.log("dbInstance: ${INSTANCE}")
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

    abstract fun accessTokenModel() : AccessTokenModelDao
    abstract fun userModel()        : UserDao
    abstract fun testModel()        : TestModelDao


}