package com.sonin.soninandroidblankproject.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.sonin.soninandroidblankproject.R
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.databinding.ActivityNavigationBinding
import com.sonin.soninandroidblankproject.repository.DataRepository
import com.sonin.soninandroidblankproject.viewModel.NavigationViewModel
import com.sonin.soninandroidblankproject.viewModel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_navigation.*
import kotlinx.android.synthetic.main.activity_navigation.view.*
import javax.inject.Inject

class NavigationActivity : AppCompatActivity() {

    private  lateinit var navigationViewModel           : NavigationViewModel
    private  lateinit var activityNavigationBinding     : ActivityNavigationBinding

    @Inject
    lateinit var dataRepository: DataRepository

    @Inject lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MyApp.instance.retrofitComponent?.inject(this)
        initBinding()
        initToolbar()
        setDataSourceObservers()
        initVars()
    }

    private fun initToolbar(){

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.tvToolbarTitle.text = "Navigation"
        supportActionBar?.setDisplayShowHomeEnabled(true);
        supportActionBar?.setHomeButtonEnabled(true);
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

    }

    fun setDataSourceObservers(){

        navigationViewModel.arrUsers.observe(this, Observer {

            it?.let {

                println("From Database ${it}")

            }

        })

    }

    fun initVars(){

        btnGetUserNet.setOnClickListener {  navigationViewModel.getUserRequest() }
        btnDeleteUser.setOnClickListener {  navigationViewModel.deleteUser() }
        btnGetUserDB.setOnClickListener  {  navigationViewModel.loadFromDb() }
        btnClearTexts.setOnClickListener {  navigationViewModel.clear() }

    }

    fun initBinding(){

        activityNavigationBinding   = DataBindingUtil.setContentView(this, R.layout.activity_navigation);
        navigationViewModel         = ViewModelProviders.of(this, viewModelFactory).get(NavigationViewModel::class.java)

        navigationViewModel.setCallingActivityFragment(this)
        activityNavigationBinding.navigationViewModel = navigationViewModel


    }

    override fun onDestroy() {
        navigationViewModel.onDestroy()
        super.onDestroy()
    }

    override fun onResume() {
        navigationViewModel.onResume()
        super.onResume()
    }

}
