package com.sonin.soninandroidblankproject.models

import android.app.Application
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.AsyncTask
import com.google.gson.annotations.SerializedName
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.database.AppDatabase
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import java.io.Serializable

/**
 * Created by athanasios.moutsioul on 19/10/2017.
 */
@Entity(tableName = "User")
data class User (
        @PrimaryKey(autoGenerate = false)
        @SerializedName("id")
        var id :Int,

        @SerializedName("name")
        var name :String?,

        @SerializedName("email")
        var email :String,

        @SerializedName("created_at")
        var created_at :String


):Serializable{

        fun deleteItem(appDatabase : AppDatabase) {
               // deleteAsyncTask(appDatabase).execute(this)
                doAsync {
                        appDatabase.userModel().deleteUser(this@User)
                }

        }

        fun insertItem(appDatabase : AppDatabase){

                doAsync {
                        appDatabase.userModel().addUser(this@User)
                }

               // insertAsyncTask(appDatabase).execute(this)
        }

//        private class deleteAsyncTask internal constructor(private val db: AppDatabase) : AsyncTask<User, Void, Void>() {
//
//                override fun doInBackground(vararg params: User): Void? {
//                        db.userModel().deleteUser(params[0])
//                        return null
//                }
//
//        }
//
//        private class insertAsyncTask internal constructor(private val db: AppDatabase) : AsyncTask<User, Void, Void>() {
//
//                override fun doInBackground(vararg params: User): Void? {
//                        Single.fromCallable {
//                                db.userModel().addUser(params[0])
//
//                        }.subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread()).subscribe()
//                        return null
//                }
//
//        }


}