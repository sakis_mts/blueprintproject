package com.sonin.soninandroidblankproject.dagger

import com.sonin.soninandroidblankproject.networking.NetworkRequests
import com.sonin.soninandroidblankproject.networking.OAuthService
import com.sonin.soninandroidblankproject.repository.DataRepository
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton
import com.sonin.soninandroidblankproject.dagger.OBSERVER_ON
import com.sonin.soninandroidblankproject.dagger.SUBCRIBER_ON
import javax.inject.Inject

@Module
class DataRepoModule  {

    @Provides
    @Singleton
    fun provideDatarRepository(@Named("ServiceRetrofitTest") testServiceApi: NetworkRequests, networkServiceApi: NetworkRequests, oauthServiceApi: OAuthService,@Named(OBSERVER_ON) observeOn: Scheduler, @Named(SUBCRIBER_ON) subscribeOn: Scheduler )
            = DataRepository(testServiceApi,networkServiceApi,oauthServiceApi, observeOn, subscribeOn)
}