package com.sonin.soninandroidblankproject.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.sonin.soninandroidblankproject.models.TestModel


@Dao
interface TestModelDao {

    @Query("select * from testmodel")
    fun getAllTestModels(): LiveData<List<TestModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTest(testModel: TestModel)

    @Delete
    fun deleteTest(testModel: TestModel)
}