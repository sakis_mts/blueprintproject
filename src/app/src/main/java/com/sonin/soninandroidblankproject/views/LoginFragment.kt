package com.sonin.soninandroidblankproject.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.sonin.soninandroidblankproject.R
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.databinding.FragmentLoginBinding
import com.sonin.soninandroidblankproject.utils.TangerineLog
import com.sonin.soninandroidblankproject.validation.DefaultNotEmptyPolicy
import com.sonin.soninandroidblankproject.validation.Field
import com.sonin.soninandroidblankproject.validation.Form
import com.sonin.soninandroidblankproject.viewModel.LoginViewModel
import com.sonin.soninandroidblankproject.viewModel.ViewModelFactory

import kotlinx.android.synthetic.main.fragment_login.*
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

/**
 * Created by athanasios.moutsioul on 10/04/2018.
 */
class LoginFragment: Fragment(),  View.OnClickListener {

    lateinit var loginViewModel                 : LoginViewModel
    lateinit var loginFragmentBinding           : FragmentLoginBinding
    private val form                            : Form by lazy { Form() }

    @Inject lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }

        MyApp.instance.retrofitComponent?.inject(this)
        initViewModel()

        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        loginFragmentBinding.loginViewModel = loginViewModel

        setHasOptionsMenu(true)
        return loginFragmentBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initVars()
        initForm()

    }


    fun initViewModel(){

        loginViewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)


    }

    private fun initForm() {

        form.addField(Field.FieldBuilder(etUsername, Field.FieldType.TEXT).policy(DefaultNotEmptyPolicy()).createField())
        form.addField(Field.FieldBuilder(etPassword, Field.FieldType.PASSWORD).policy(DefaultNotEmptyPolicy()).createField())
        form.setCustomFailure { view, s ->

            when (view.id){

                R.id.etUsername -> { etUsername.error = resources.getString(R.string.register_email_validation) }
                R.id.etPassword -> { etPassword.error = resources.getString(R.string.register_password_validation) }

            }

        }
    }

    fun initVars(){

      //  val typeFace = ResourcesCompat.getFont(activity, R.font.avenir_heavy);
       // etPasswordLayout.setTypeface(typeFace)
        btnLogin.setOnClickListener(this)
        btnRegister.setOnClickListener(this)
        tvForgot.setOnClickListener(this)

        setSingleLiveDataEvents()

    }


    override fun onClick(view: View) {

        when (view.id){
            R.id.btnLogin  -> {

                if (etUsername.text.isEmpty()){

                    etUsername.error = resources.getString(R.string.register_email_validation)

                }else if (etPassword.text.isEmpty()){

                    etPassword.error = resources.getString(R.string.register_password_validation)

                }else{

                    clearInputErrorTips()
                    loginViewModel.logingRequest()

                }

            }
            R.id.tvForgot       -> { }
            R.id.btnRegister    -> { }
            else ->{
                TangerineLog.log("No Action Selected")}

        }

    }

    fun clearInputErrorTips(){

        etUsername.error = null
        etPassword.error = null

    }


    fun setSingleLiveDataEvents(){

        loginViewModel.showErrorToast.observe(this, Observer {

            toast(it.toString())

        })

    }


}