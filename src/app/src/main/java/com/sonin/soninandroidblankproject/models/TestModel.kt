package com.sonin.soninandroidblankproject.models

import android.app.Application
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.AsyncTask
import com.google.gson.annotations.SerializedName
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.database.AppDatabase
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import java.io.Serializable

/**
 * Created by athanasios.moutsioul on 19/10/2017.
 */
@Entity(tableName = "testmodel")
data class TestModel (
        @PrimaryKey(autoGenerate = false)
        @SerializedName("idGeneralIssue")
        var idGeneralIssue :Int,

        @SerializedName("strIssue")
        var strIssue :String?,

        @SerializedName("idDriver")
        var idDriver :Int



):Serializable{

    fun deleteItem(appDatabase : AppDatabase) {

        doAsync {
            appDatabase.testModel().deleteTest(this@TestModel)
        }

    }

    fun insertItem(appDatabase : AppDatabase){

        doAsync {
            appDatabase.testModel().addTest(this@TestModel)
        }

    }


}