package com.sonin.soninandroidblankproject.views

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sonin.soninandroidblankproject.R
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.viewModel.LoginViewModel
import com.sonin.soninandroidblankproject.viewModel.ViewModelFactory
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setFragment(LoginFragment())

    }

    fun setFragment(fragment: LoginFragment){

        supportFragmentManager.beginTransaction().replace(R.id.frame_container,fragment ,"Loginfragment").commit()
    }
}
