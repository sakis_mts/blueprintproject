package com.sonin.soninandroidblankproject

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.databinding.ObservableField
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.filters.MediumTest
import android.support.test.runner.AndroidJUnit4
import com.sonin.soninandroidblankproject.viewModel.LoginViewModel
import com.sonin.soninandroidblankproject.views.LoginActivity
import com.sonin.soninandroidblankproject.views.LoginFragment
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

@MediumTest
@RunWith(AndroidJUnit4::class)
class ClassLoginFragment {

    @Rule
    @JvmField
    val activityRule = IntentsTestRule(LoginActivity::class.java)

    @Rule
    @JvmField
    val taskExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var viewModel: LoginViewModel

    private val emailField = ObservableField<String>()
    private val passwordField = ObservableField<String>()


    private lateinit var loginFragment: LoginFragment

    @Before
    fun setUp() {
        loginFragment = LoginFragment()

        `when`(viewModel.obsUsername).thenReturn(emailField)


        activityRule.activity.setFragment(loginFragment)
    }

    @Test
    fun emailAndPasswordAreEmptyInitially() {
      //  onView(withId(R.id.etUsername)).check(matches(withText("")))
    }

}