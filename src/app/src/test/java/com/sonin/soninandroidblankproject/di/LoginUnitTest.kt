package com.sonin.soninandroidblankproject.di

import android.os.Build
import com.sonin.soninandroidblankproject.BaseTest
import org.junit.Rule
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.FrameLayout
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.R
import com.sonin.soninandroidblankproject.viewModel.LoginViewModel
import com.sonin.soninandroidblankproject.views.LoginActivity
import com.sonin.soninandroidblankproject.views.LoginFragment
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.robolectric.Robolectric
import  org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment;
import java.net.HttpURLConnection
import kotlin.test.assertNotNull
import android.widget.TextView
import com.sonin.soninandroidblankproject.TrampolineSchedulerRule
import org.robolectric.Shadows
import kotlin.test.assertTrue
import android.content.ComponentName
import android.support.design.widget.TextInputEditText
import android.widget.Button
import com.sonin.soninandroidblankproject.application.MyApp
import com.sonin.soninandroidblankproject.views.NavigationActivity
import org.junit.Assert.assertThat
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowIntent
import org.robolectric.shadows.ShadowToast
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])

class LoginUnitTest : BaseTest(){

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule() // Force tests to be executed synchronously

    @get:Rule
    val rule = TrampolineSchedulerRule()

    private lateinit var activity   : AppCompatActivity
    private lateinit var fragment   : Fragment
    private lateinit var viewModel  : LoginViewModel

    override fun isMockServerEnabled(): Boolean = true

    @Before
    override fun setUp(){
        super.setUp()

        this.activity   = Robolectric.setupActivity(AppCompatActivity::class.java)
        this.viewModel  = ViewModelProviders.of(this.activity, viewModelFactory)[LoginViewModel::class.java]

    }

    @Test
    fun isFragmentVisible(){
        val activity  = Robolectric.buildActivity( LoginActivity::class.java )
                .create()
                .resume()
                .get()
        this.fragment = activity.supportFragmentManager.findFragmentByTag("Loginfragment")
        assertNotNull(fragment)

    }

    @Test
    fun isEmailEmpty(){
        val activity  = Robolectric.buildActivity( LoginActivity::class.java )
                .create()
                .resume()
                .get()
        this.fragment   = activity.supportFragmentManager.findFragmentByTag("Loginfragment")
        val btnLogin    = fragment.view?.findViewById<Button>(R.id.btnLogin)
        val etEmail     = fragment.view?.findViewById<TextInputEditText>(R.id.etUsername)

        etEmail?.setText("")
        btnLogin?.performClick()
        assertEquals("Email cannot be empty",etEmail?.error)

    }


    @Test
    fun isPasswordEmpty(){
        val activity  = Robolectric.buildActivity( LoginActivity::class.java )
                .create()
                .resume()
                .get()
        this.fragment   = activity.supportFragmentManager.findFragmentByTag("Loginfragment")
        val btnLogin    = fragment.view?.findViewById<Button>(R.id.btnLogin)
        val etPass      = fragment.view?.findViewById<TextInputEditText>(R.id.etPassword)
        val etEmail     = fragment.view?.findViewById<TextInputEditText>(R.id.etUsername)

        etEmail?.setText("athan")
        etPass?.setText("")
        etPass?.requestFocus()

        btnLogin?.performClick()
        assertEquals("Password cannot be empty",etPass?.error)

    }


    @Test
    fun checkObservableFields(){

        val activity  = Robolectric.buildActivity( LoginActivity::class.java )
                .create()
                .resume()
                .get()

        this.fragment = activity.supportFragmentManager.findFragmentByTag("Loginfragment")
        (this.fragment as LoginFragment).loginViewModel.obsUsername.set("Athanasios")
        (this.fragment as LoginFragment).loginViewModel.obsPassword.set("Testing123")

        assertEquals("Athanasios", (this.fragment as LoginFragment).loginViewModel.obsUsername.get())
        assertEquals("Testing123", (this.fragment as LoginFragment).loginViewModel.obsPassword.get())

    }

    @Test
    fun getAccessToken_whenSuccess() {
        // Prepare data
      val response  = this.mockHttpResponse("getUser_whenSuccess.json", HttpURLConnection.HTTP_OK)

        this.viewModel.obsUsername.set("test")
        this.viewModel.obsPassword.set("testing")
        this.viewModel.logingRequest()


        val intent = Shadows.shadowOf(activity).peekNextStartedActivity()
        val shadowIntent = shadowOf(intent)

        println(this.viewModel.accessToken)
        // Checks
        assertEquals(this.viewModel.obsUsername.get(), "test")
        assertEquals(this.viewModel.obsPassword.get(), "testing")
        assertEquals(true, SessionManager.instance.isLoggedIn())
        assertEquals(NavigationActivity::class.java, shadowIntent.intentClass)
    }

    @Test
    fun getAccessToken_whenFailed400() {
        // Prepare data
        val response  = this.mockHttpResponse("getUser_whenSuccess.json", HttpURLConnection.HTTP_BAD_REQUEST)

        this.viewModel.obsUsername.set("test")
        this.viewModel.obsPassword.set("testing")
        this.viewModel.logingRequest()

        // Checks
        assertEquals(this.viewModel.obsUsername.get(), "test")
        assertEquals(this.viewModel.obsPassword.get(), "testing")
        assertEquals(false, SessionManager.instance.isLoggedIn())
    }

    @Test
    fun getAccessToken_whenFailed500() {
        // Prepare data
        val response  = this.mockHttpResponse("getUser_whenSuccess.json", HttpURLConnection.HTTP_BAD_GATEWAY)

        this.viewModel.obsUsername.set("test")
        this.viewModel.obsPassword.set("testing")
        this.viewModel.logingRequest()

        // Checks
        assertEquals(this.viewModel.obsUsername.get(), "test")
        assertEquals(this.viewModel.obsPassword.get(), "testing")
        assertEquals(false, SessionManager.instance.isLoggedIn())

    }


}