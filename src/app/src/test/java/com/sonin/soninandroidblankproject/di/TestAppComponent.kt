package com.sonin.soninandroidblankproject.di

import com.sonin.soninandroidblankproject.BaseTest
import com.sonin.soninandroidblankproject.dagger.AppModule
import com.sonin.soninandroidblankproject.dagger.DataRepoModule
import com.sonin.soninandroidblankproject.dagger.RetrofitModule
import javax.inject.Singleton
import dagger.Component
import io.github.philippeboisney.retrokotlin.di.ViewModelModule

@Singleton
@Component(modules = [ AppModule::class,RetrofitModule::class, DataRepoModule::class, ViewModelModule::class,  TestRxJavaModule::class])
interface TestAppComponent {

    fun inject(baseTest: BaseTest)
}